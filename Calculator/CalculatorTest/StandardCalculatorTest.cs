using Calculator; //name space is added - 
using System;
using Xunit;

namespace CalculatorTest//test project
{
    public class StandardCalculatorTest //default class
    {
        /*How to name a method
         * (1) Name of the method being tested
         * (2) scenario (action)
         * (3) expected behavior (output)
         */


        //[Fact] //Data Annocation--> for data validation
        //public void Add_AddTwoNumbers_ShouldReturnSum() //default method
        //{
        //    //1+1=2 (add two numbers)
        //    //AAA

        //    //*** Arrange ***
        //    //--create an object
        //    StandardCalculator calculator = new StandardCalculator();
        //    //to store values, we need variables
        //    int lhs = 1; //give simple values for the test
        //    int rhs = 1;
        //    int expected = lhs + rhs;

        //    //*** Act ***  (against a method)
        //    int actual = calculator.Add(lhs, rhs);


        //    //***Assert ***
        //    Assert.Equal(expected, actual);
        //}
        [Theory] //Data Annocation--> for data validation -test varios test elements
        [InlineData(1,1,2)]
        [InlineData(-1,1,0)]       
        
        public void Add_AddTwoNumbers_ShouldReturnSum(int lhs, int rhs, int expected) //default method
        {
            //1+1=2 (add two numbers)
            //AAA

            //*** Arrange ***
            //--create an object
            StandardCalculator calculator = new StandardCalculator();
            //to store values, we need variables                      
          

            //*** Act ***  (against a method)
            int actual = calculator.Add(lhs, rhs);


            //***Assert ***
            Assert.Equal(expected, actual);
        }

        [Fact] 
        public void Divide_DivideTwoNumbers_ShouldReturnDivisionOfNumber() 
        {
           
            //*** Arrange ***           
            StandardCalculator calculator = new StandardCalculator();
            int lhs = 2; 
            int rhs = 1;
            double expected = lhs / rhs;

            
            double actual = calculator.Divide(lhs, rhs);


            //***Assert ***
            Assert.Equal(expected, actual);
                }


        [Fact]
        public void Divide_WithZero_ShouldReturnDivideByZeroException()
        {

            //*** Arrange ***           
            StandardCalculator calculator = new StandardCalculator();
            int lhs = 1;
            int rhs = 0;
            
            //Act + Assert [lambda expression]
           Assert.Throws<DivideByZeroException>(()=>calculator.Divide(lhs, rhs));   
        }

    }
}
