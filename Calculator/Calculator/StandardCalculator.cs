﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    public class StandardCalculator
    {
       
        //definion for add method
        public int Add(int lhs, int rhs)
        {
            return lhs+rhs;
        }

        public double Divide(int lhs, int rhs)//method -> function
        {
            return lhs / rhs;
        }
    }
}
