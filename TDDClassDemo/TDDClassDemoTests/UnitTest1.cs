using System;
using TDDClassDemo;
using Xunit;


namespace TDDClassDemoTests
{
    public class UnitTest1
    {
        //[Fact]
        //public void CheckFizzBuzz_NumberIsMultipleOfThreeOnly_Fizz()
        //{
        //    //Arrange
        //    FizzBuzzChecker checker = new FizzBuzzChecker();
        //    int num = 3;
        //    string expected = "Fizz";

        //    //Act
        //    string actual = checker.CheckFizzBuzz(num);

        //    //Assert
        //    Assert.Equal(expected, actual);
        //}
        [Fact]
        public void CheckFizzBuzz_NumberIsMultipleOfFiveOnly_Buzz()
        {
            //Arrange
            FizzBuzzChecker checker = new FizzBuzzChecker();
            int num = 5;
            string expected = "Buzz";

            //Act
            string actual = checker.CheckFizzBuzz(num);

            //Assert
            Assert.Equal(expected, actual);
        }
        [Theory]
        [InlineData(3)]
        [InlineData(6)]
        [InlineData(9)]
        public void CheckFizzBuzz_NumberIsMultipleOfThreeOnly_Fizz(int num)
        {
            //Arrange
            FizzBuzzChecker checker = new FizzBuzzChecker();
            
            string expected = "Fizz";

            //Act
            string actual = checker.CheckFizzBuzz(num);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CheckFizzBuzz_NumberIsNotMultipleOf3or5_NumberAsString()
        {
            //Arrange
            FizzBuzzChecker checker = new FizzBuzzChecker();
            int num = 1;//this is an integer
            string expected = "1";//this is a string

            //Act
            string actual = checker.CheckFizzBuzz(num);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CheckFizzBuzz_NumberIsMultipleOf3and5_FizzBuzz()
        {
            //Arrange
            FizzBuzzChecker checker = new FizzBuzzChecker();
            int num = 15;
            string expected = "FizzBuzz";

            //Act
            string actual = checker.CheckFizzBuzz(num);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CheckFizzBuzz_NumberIsNegative_ThrowsInvalidNumberException()
        {
            //Arrange
            FizzBuzzChecker checker = new FizzBuzzChecker();
            int num = -1;            

            //Act & Assert
            Assert.Throws<InvalidFizzBuzzException>(() => checker.CheckFizzBuzz(num));
                       
        }

    }
}
