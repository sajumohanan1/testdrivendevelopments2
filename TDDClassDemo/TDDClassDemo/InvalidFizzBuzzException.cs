﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDDClassDemo
{
    public class InvalidFizzBuzzException : Exception //Inherit features from Exception class (parent class) to the child class
    {
        public override string Message => "FizzBuzz checker does not accept this number";
    }
}
