﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDDClassDemo
{
    public class FizzBuzzChecker
    {
        public string CheckFizzBuzz(int num)
        {
            if (num % 15 == 0)
            {
                return "FizzBuzz";
            }
            if (num % 3 == 0)//condition must be true
            {
                //when condition true, perform action part
                return "Fizz";
            }
            if (num % 5 == 0)
            {                
                return "Buzz";
            }
            if (num < 0)
            {
                throw new InvalidFizzBuzzException();
            }
            return num.ToString();
            
        }
    }
}
